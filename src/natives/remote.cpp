






#include "../main.h"
#include "../Natives.h"

extern bool	bRconSocketReply;
extern SOCKET	cur_sock;
extern void RconSocketReply(const char* szMessage);
extern std::string remote_password;

// native SetRemotePassword(const password[]);
AMX_DECLARE_NATIVE(Natives::SetRemotePassword) {
	CHECK_PARAMS(1, NO_FLAGS);

	CScriptParams::Get()->Read(&remote_password);
	return 1;
}

// native SendRemoteReply(const message[]);
AMX_DECLARE_NATIVE(Natives::SendRemoteReply) {
	CHECK_PARAMS(1, NO_FLAGS);

	std::string message;
	CScriptParams::Get()->Read(&message);

	if (cur_sock == INVALID_SOCKET) return 0;

	bRconSocketReply = true;
	RconSocketReply(message.c_str());
	bRconSocketReply = false;
	return 1;
}



